from scripts.prescript_setup import db, ConfigProxy
from scripts.backsync.agents import MoBacksyncAgent
from scripts.backsync.brokers import BasicBacksyncBroker


def run_agents(dry_run_override=None):
    dry_run_mode = dry_run_override if isinstance(dry_run_override, bool) else ConfigProxy.get_backsync_dry_run_only()
    broker_instance = BasicBacksyncBroker()
    for cls in [MoBacksyncAgent]:
        agent_instance = cls(backsync_broker=broker_instance, dry_run=dry_run_mode)
        agent_instance.sync()


if __name__ == '__main__':
    run_agents()
