from scripts.prescript_setup import db
from icms_orm.cmspeople import Person, MemberStatusValues as Status
from icms_orm.common import PrehistoryTimeline as PL
from icms_orm.epr import TimeLineUser as TLU
import sqlalchemy as sa
import logging
import sys
from datetime import date, datetime


class ExYearWizard(object):

    EXMEMBER_STATUSES = [Status.PERSON_STATUS_DECEASED, Status.PERSON_STATUS_EXMEMBER, Status.PERSON_STATUS_CMSEXTENDED]
    # delay tolerance must be reasonably high to account for sync hiccups or initial granularity of imported data
    EPR_DELAY_TOLERANCE = 28

    @staticmethod
    def resolve_mismatch(stored, parsed, epr):
        if parsed is not None and epr is not None and (epr.date() - parsed).days <= ExYearWizard.EPR_DELAY_TOLERANCE:
            # Corroborated by EPR, parsed date is highly credible
            return parsed
        if epr is not None and stored is not None and (epr.date() - stored).days <= ExYearWizard.EPR_DELAY_TOLERANCE:
            # Also highly reliable, for the same reason
            return stored
        if stored is None:
            # Then when we have nothing and seek the better of two anythings
            if parsed is None and epr is None:
                return date.today()
            return epr or parsed
        else:
            # Or we have something which is either impossible to verify against EPR or contradicts it.
            if epr is None:
                # so it's stored vs parsed and without means to verify - let's leave it as it is
                return stored
            else:
                # here it would seem that the date in DB is just too distant from corresponding EPR entry. EPR wins!
                return epr

    @staticmethod
    def main(dry_run=False):
        logging.info('Starting the script with dry_run={0}'.format(dry_run))
        departures = {r[0]: r[1] for r in db.session.query(Person.cmsId, Person.exDate).filter(
            Person.status.in_(ExYearWizard.EXMEMBER_STATUSES)).all()}

        parsed_departures = {r[0]: r[1] for r in db.session.query(PL.cms_id, sa.func.max(PL.end_date)).
            filter(sa.and_(PL.status_cms.notin_(ExYearWizard.EXMEMBER_STATUSES), PL.status_cms.ilike('CMS%'))).
            group_by(PL.cms_id).all()}

        # subquery to partition the timelines by CMS ID and rank by their timestamps
        sq = db.session.query(
            TLU.cmsId.label('tlu_cms_id'),
            TLU.id.label('tlu_id'),
            sa.func.rank().over(order_by=[sa.desc(TLU.year), sa.desc(TLU.timestamp)],
                                partition_by=[TLU.cmsId]).label('rnk')
            ).subquery()
        # subquery's alias (for a join against itself)
        sq2 = sa.orm.aliased(sq)
        # then a pairing subquery that should yield the id of one timeline, along with its predecessor's
        pairing_sq = db.session.query(sq.c.tlu_cms_id, sq.c.tlu_id.label('tlu_id_1'), sq2.c.tlu_id.label('tlu_id_2')).\
            join(sq2, sa.and_(sq.c.tlu_cms_id == sq2.c.tlu_cms_id, sq.c.rnk == sq2.c.rnk - 1)).\
            subquery()
        # aliasing TLU for TLU-sq-TLU join
        TLU2 = sa.orm.aliased(TLU)
        # then just plug the TLUs where their IDs stand and select the most recent row per user
        q = db.session.query(TLU.cmsId, sa.func.max(TLU.timestamp)).\
            join(pairing_sq, TLU.id == pairing_sq.c.tlu_id_1).join(TLU2, TLU2.id == pairing_sq.c.tlu_id_2).\
            filter(TLU.status.in_(ExYearWizard.EXMEMBER_STATUSES)).filter(sa.and_(
                TLU2.status.notin_(ExYearWizard.EXMEMBER_STATUSES), TLU2.status.ilike('CMS%'))).group_by(TLU.cmsId)

        epr_departures = {r[0]: r[1] for r in q.all()}

        skipped = 0
        matching = 0
        needed_resolution = 0
        changed = 0

        for cms_id, stored in departures.items():
            deduced = parsed_departures.get(cms_id)
            epr_departure = epr_departures.get(cms_id)
            if deduced is None and epr_departure is None:
                skipped += 1
                continue
            elif str(deduced) == stored:
                matching += 1
                continue
            elif stored is None or str(deduced) != stored:
                if deduced is None and epr_departure is None:
                    logging.warning('3 null dates for CMS ID {0}!'.format(cms_id))
                needed_resolution += 1
                epr_departure = epr_departures.get(cms_id)
                resolved_date = ExYearWizard.resolve_mismatch((lambda x: x and datetime.strptime(x, '%Y-%m-%d').date() or None)(stored), deduced, epr_departure)
                if str(resolved_date) != stored:
                    changed += 1
                    logging.info('Deprature for CMS ID {0} to be updated from {1} to {2}. Parsed was: {3}, EPR was: {4}'
                                 .format(cms_id, stored, resolved_date, deduced, epr_departure))
                    if not dry_run:
                        db.session.query(Person).filter(Person.cmsId == cms_id).update({Person.exDate: deduced})

        logging.info('{0} cases considered, {1} times dates matched, {2} cases skipped (no alternative ideas), '
                     '{3} cases needed resolution, {4} dates have been updated'
                     .format(len(departures), matching, skipped, needed_resolution, changed))
        if not dry_run:
            db.session.commit()


if __name__ == '__main__':
    ExYearWizard.main(dry_run=any([x in map(str.lower, sys.argv) for x in ['dry', 'dryrun', 'dry-run']]))
