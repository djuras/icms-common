import time
import json
import logging
import traceback
from datetime import date
from .brokers import ChangeLogAwareBroker, DefaultBroker
from scripts.prescript_setup import config, db, ConfigProxy
from icms_orm.common import ApplicationAsset
from datetime import datetime
from .agents import BaseSyncAgent


class SyncRunInfo(object):

    def __init__(self):
        self._timestamps = {}

    def __restore_values_from_json_column(self, data):
        self._timestamps = {k: datetime.fromtimestamp(v) for k, v in data['timestamps'].items()}

    @classmethod
    def __agent_or_class_to_key(cls, agent_or_class):
        if isinstance(agent_or_class, BaseSyncAgent):
            return agent_or_class.__class__.__name__
        elif isinstance(agent_or_class, str):
            return agent_or_class
        elif isinstance(agent_or_class, type):
            return agent_or_class.__name__
        else:
            raise TypeError('Provided agent/class does not match any of the supported types!')

    def store_agent_timestamp(self, agent_or_class, timestamp=None):
        # removing the sub-second part of the timestamp as that's how it ends up being now anyway (roundtrip conv.)
        timestamp = timestamp or datetime.now()
        timestamp.replace(microsecond=0)
        self._timestamps[SyncRunInfo.__agent_or_class_to_key(agent_or_class)] = timestamp

    def get_timestamp(self, agent_or_class):
        return self._timestamps.get(SyncRunInfo.__agent_or_class_to_key(agent_or_class), None)

    def prepare_for_storage(self):
        pass

    @classmethod
    def get_last_run_info_from_db(cls):
        data = db.session.query(ApplicationAsset.data).filter(ApplicationAsset.name == 'fwd_sync_last_run_info').\
            one_or_none()
        if data:
            obj = cls()
            obj.__restore_values_from_json_column(data[0])
            return obj
        return None

    def store_in_db(self):
        # prepare the info object to be insertable into the assets table
        dict_data = vars(self)
        dict_data['timestamps'] = dict_data.pop('_timestamps')
        for key, value in dict_data['timestamps'].items():
            if isinstance(value, datetime):
                dict_data['timestamps'][key] = time.mktime(value.timetuple())
        # grab the existing row if possible
        row = db.session.query(ApplicationAsset).filter(ApplicationAsset.name == 'fwd_sync_last_run_info').one_or_none()
        if row is None:
            row = ApplicationAsset.from_ia_dict({
                ApplicationAsset.name: 'fwd_sync_last_run_info',
                ApplicationAsset.application: 'common',
                ApplicationAsset.data: dict_data
            })
        else:
            row.data = dict_data
        db.session.add(row)
        db.session.commit()


class ForwardSyncManager(object):

    @classmethod
    def __get_broker_instance(cls, broker_class_override=None):
        broker_class = broker_class_override or DefaultBroker.get_subclass_by_name(ConfigProxy.get_sync_broker_class_name())
        return broker_class()

    @classmethod
    def launch_sync(cls, agent_classes_override=None, broker_class_override=None):
        broker = cls.__get_broker_instance(broker_class_override=broker_class_override)
        sync_agents = []
        for agent_class in agent_classes_override:
            sync_agents.append(agent_class(broker))

        # pull the last run's info from the DB or create an instance if this is the first time
        sync_info = SyncRunInfo.get_last_run_info_from_db() or SyncRunInfo()

        try:
            for sync_agent in sync_agents:
                if isinstance(broker, ChangeLogAwareBroker):
                    _agent_last_run = sync_info.get_timestamp(sync_agent)
                    logging.debug('{0} sync agent previous ran on: {1}'.format(sync_agent.__class__, _agent_last_run))
                    broker.set_since_when(_agent_last_run)
                launch_timestamp = datetime.now()
                sync_agent.sync()
                # store the timestamp AFTER the agent is done running.
                # We use the same agent instance so that earlier timestamps are preserved in case of failure.
                sync_info.store_agent_timestamp(sync_agent, launch_timestamp)
            logging.info('Sync success!')
        except Exception as e:
            logging.error('Sync encountered a problem: %s' % str(e) )
            logging.error(traceback.format_exc())
        finally:
            sync_info.store_in_db()
            logging.info('Sync procedure terminates')
