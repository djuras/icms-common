#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

def hack_config(config):
    """
    Please provide a more useful implementation in your script, before importing anything specific from this module.
    E.g.:

        import prescript_setup
        prescript_setup.hack_config(config):
            config['db']['main_bind'] = 'epr'
    :return:
    """
    pass
