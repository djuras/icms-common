#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.icmstest.mock import mock_date_param_name, today, date
