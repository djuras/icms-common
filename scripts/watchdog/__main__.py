from scripts.prescript_setup import db, config
from scripts.watchdog.units import run_watchdogs

if __name__ == '__main__':
    run_watchdogs()
