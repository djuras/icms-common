import logging
import traceback
from typing import Type

import sqlalchemy as sa
from datetime import date, datetime
from scripts.prescript_setup import db
from scripts.watchdog.units.watchdog_base import AutonomousWatchdogBase, WatchdogRemedy
from icms_orm.epr import TimeLineUser as TLU, Category, EprUser, TimeLineInst as TLI
from icms_orm.common import EprAggregate, EprAggregateUnitValues
from icms_orm.cmspeople import MemberStatusValues as MSV, InstStatusValues as ISV, Person


class ApplicantDueRemedy(WatchdogRemedy):

    def apply(self):
        ssn = TLU.session()
        time_lines = ssn.query(TLU).join(Category, TLU.category == Category.id).\
            filter(sa.and_(
            TLU.cmsId == self._cms_id, TLU.year == self._year, TLU.timestamp >= sa.func.coalesce(self._application_start, datetime(self._year, 1, 1, 0, 0)),
            TLU.status == MSV.PERSON_STATUS_CMS, TLU.isSuspended == False, Category.authorOK == True
        )).all()
        sum_of_fractions = sum([_tl.yearFraction for _tl in time_lines])
        for _tl in time_lines:
            assert isinstance(_tl, TLU)
            _tl.dueApplicant = self._correct_due * (_tl.yearFraction / sum_of_fractions)
            ssn.add(_tl)

    def __init__(self, cms_id, year, correct_app_due, application_start) -> None:
        super().__init__()
        self._cms_id = cms_id
        self._year = year
        self._correct_due = correct_app_due
        self._application_start = application_start


class ApplicantDueWatchdog(AutonomousWatchdogBase):

    def __init__(self, epr_year=None):
        super(ApplicantDueWatchdog, self).__init__()
        self._epr_year = epr_year or date.today().year
        self._unblock_dates = dict()
        self._block_dates = dict()
        self._author_blocks = dict()
        self._inst_statuses = dict()
        self._affiliations = dict()

        self.__init_author_block_data()
        self.__init_app_start_data()
        self.__init_billed_dues_data()
        self.__init_predating_credit_data()
        self.__init_inst_status_data()
        self.__init_affiliation_data()

    def __init_author_block_data(self):
        ssn = db.session
        block_q = ssn.query(Person.cmsId, Person.dateAuthorBlock, Person.dateAuthorUnblock,Person.isAuthorBlock).\
            filter(Person.status == MSV.PERSON_STATUS_CMS)
        for _cms_id, _date_block, _date_unblock, is_blocked in block_q.all():
            if _date_block:
                self._block_dates[_cms_id] = _date_block
            if _date_unblock:
                self._unblock_dates[_cms_id] = _date_unblock
            self._author_blocks[_cms_id] = is_blocked

    def __init_app_start_data(self):
        ssn = db.session
        sq = ssn.query(
            TLU.id.label('current'),
                       sa.func.lag(TLU.id, 1).over(partition_by=TLU.cmsId, order_by=TLU.timestamp).label('previous')).\
            subquery()

        PrevTLU = sa.orm.aliased(TLU)
        PrevCategory = sa.orm.aliased(Category)
        LatestCategory = sa.orm.aliased(Category)
        PrevTLI = sa.orm.aliased(TLI)

        q = ssn.query(TLU.cmsId, TLU.timestamp, TLI.timestamp, PrevTLU.timestamp, PrevTLI.timestamp). \
            join(sq, TLU.id == sq.c.current). \
            join(Category, TLU.category == Category.id). \
            join(TLI, sa.and_(TLU.instCode == TLI.code, TLU.year == TLI.year, TLU.timestamp >= TLI.timestamp)). \
            join(PrevTLU, sq.c.previous == PrevTLU.id, isouter=True). \
            join(PrevCategory, PrevTLU.category == PrevCategory.id, isouter=True). \
            join(PrevTLI, sa.and_(PrevTLU.instCode == PrevTLI.code, PrevTLU.year == PrevTLI.year, PrevTLI.timestamp >= PrevTLU.timestamp), isouter=True)
        # WARNING: join in the latest user data to ensure that they're still applicants
        q = q.join(EprUser, sa.and_(EprUser.cmsId == TLU.cmsId, EprUser.isSuspended == TLU.isSuspended, EprUser.status == TLU.status)).\
            join(LatestCategory, sa.and_(EprUser.category == LatestCategory.id, LatestCategory.authorOK == True))
        q = q.filter(sa.and_(
            TLI.cmsStatus == ISV.YES,
            TLU.status == MSV.PERSON_STATUS_CMS, Category.authorOK == True, TLU.isSuspended == False, TLU.isAuthor == False,
            sa.or_(PrevTLU.status == None, PrevTLU.status != 'CMS', PrevTLU.isSuspended == True, PrevTLU.isSuspended == None,
                   PrevCategory.authorOK == False, PrevCategory.authorOK == None, PrevTLI.cmsStatus != ISV.YES)
        ))
        # CRITICAL: there might be many TLIs for given inst-year combo - here we create windows to prefer most recent
        # TLIs wrt TLU's timestamp
        q = q.order_by(TLU.cmsId, sa.desc(TLU.timestamp), TLU.timestamp-TLI.timestamp, PrevTLU.timestamp - PrevTLI.timestamp).distinct(TLU.cmsId).from_self(TLU.cmsId, TLU.timestamp)

        self._app_starts = dict()
        rows = q.all()
        for _cms_id, _estimated_start in rows:
            if self.was_blocked_on(_cms_id, _estimated_start):
                self._app_starts[_cms_id] = _estimated_start
            else:
                _date_unblocked = self.get_unblock_date(_cms_id)
                if _date_unblocked is not None and _date_unblocked.year in [self._epr_year, self._epr_year - 1]:
                    # todo: unblocked this or past year - we should check to rule out recent app start
                    self.add_issue('APP START NEEDS FINE CHECKING: CMS ID {0}, estimated start: {1}, unblock: {2}'.
                                   format(_cms_id, _estimated_start.date(), _date_unblocked))
                self._app_starts[_cms_id] = None

    def __init_billed_dues_data(self):
        ssn = db.session
        current_year_dues_query = ssn.query(TLU.cmsId, sa.func.sum(TLU.dueApplicant)). \
            filter(TLU.year == self._epr_year).group_by(TLU.cmsId)

        self._applied_dues = {_cms_id: _dues for _cms_id, _dues in current_year_dues_query.all()}

    def __init_predating_credit_data(self):
        ssn = db.session
        q = ssn.query(EprAggregate.cms_id, EprAggregate.year, sa.func.sum(EprAggregate.aggregate_value)).\
            filter(sa.or_(EprAggregate.aggregate_unit == EprAggregateUnitValues.PLEDGES_DONE, EprAggregate.aggregate_unit == EprAggregateUnitValues.SHIFTS_DONE)).\
            group_by(EprAggregate.cms_id, EprAggregate.year)

        self._credits = dict()
        for _cms_id, _year, _credit in q.all():
            self._credits[_cms_id] = self._credits.get(_cms_id, dict())
            self._credits[_cms_id][_year] = _credit

    def __init_inst_status_data(self):
        ssn = db.session
        q = ssn.query(TLI.code, TLI.cmsStatus).filter(TLI.year == self._epr_year).order_by(TLI.code, sa.desc(TLI.timestamp)).distinct(TLI.code)
        self._inst_statuses = {_code: _status for _code, _status in q.all()}

    def __init_affiliation_data(self):
        ssn = db.session
        q = ssn.query(TLU.cmsId, TLU.instCode).filter(TLU.year == self._epr_year).order_by(TLU.cmsId, sa.desc(TLU.timestamp)).distinct(TLU.cmsId)
        self._affiliations = {_cms_id: _code for _cms_id, _code in q.all()}

    @property
    def applicant_cms_ids(self):
        return self._app_starts.keys()

    def get_inst_status(self, code):
        return self._inst_statuses.get(code)

    def get_affiliation(self, cms_id):
        return self._affiliations.get(cms_id)

    def get_app_start_date(self, cms_id):
        return (lambda x: isinstance(x, datetime) and x.date() or None)(self._app_starts.get(cms_id))

    def get_dues(self, cms_id):
        return self._applied_dues.get(cms_id, 0)

    def get_credit(self, cms_id, year):
        return self._credits.get(cms_id, {}).get(year, 0)

    def get_credit_predating_application(self, cms_id):
        total = 0
        _app_start = self.get_app_start_date(cms_id)
        _app_start_year = _app_start is not None and _app_start.year or 0
        for year, sub_credit in self._credits.get(cms_id, {}).items():
            if year < _app_start_year:
                total += sub_credit
        return total

    def get_app_target(self, cms_id):
        return 6

    def get_amortized_app_target(self, cms_id):
        return self.get_app_target(cms_id) - self.get_credit_predating_application(cms_id)

    def get_expected_this_year_due(self, cms_id):
        _app_start = self.get_app_start_date(cms_id=cms_id)
        if _app_start is not None and _app_start.year in [self._epr_year - x for x in (0, 1)]:
            # that is the fraction carried over to the next year
            _fraction = (_app_start - date(_app_start.year, 1, 1)).days / 365
            if _app_start.year == self._epr_year:
                _fraction = 1 - _fraction
            return max(0, self.get_amortized_app_target(cms_id=cms_id) * _fraction)
        return 0

    def get_block_date(self, cms_id):
        return self._block_dates.get(cms_id)

    def get_unblock_date(self, cms_id):
        return self._unblock_dates.get(cms_id)

    def is_blocked(self, cms_id):
        return self._author_blocks.get(cms_id)

    def was_blocked_on(self, cms_id, request_date):
        request_date = request_date.date() if isinstance(request_date, datetime) else request_date
        _date_block = self.get_block_date(cms_id)
        _date_unblock = self.get_unblock_date(cms_id)
        _is_blocked = self.is_blocked(cms_id)

        if _is_blocked:
            if _date_block is None or _date_block <= request_date:
                return True
        else:
            if _date_unblock is not None and _date_unblock >= request_date:
                return True
        return False

    def add_issue_for(self, subject_str, issue_str):
        self.add_issue('{subject:<5}: {issue} [{url}]'.format(subject=subject_str, issue=issue_str,
                                           url='https://icms.cern.ch/tools/users/profile/{0}'.format(subject_str)))

    def check(self):
        pass


class RoamingAppDueWatchdog(ApplicantDueWatchdog):
    def __init__(self, epr_year=None):
        super().__init__(epr_year=epr_year)

    def check(self):
        mistreated_applicant_ids = set()
        for _cms_id, _billed_due in self._applied_dues.items():
            if self.get_dues(_cms_id) > 0:
                _app_start = self._app_starts.get(_cms_id)
                _issue = None
                if _app_start is None:
                    _ic = self.get_affiliation(_cms_id)
                    _inst_status = self.get_inst_status(_ic)
                    _issue = '{total:.2f} EPR due in {year} but no application start found [{ic}, {inst_status}]!'.format(
                        total=self.get_dues(_cms_id), year=self._epr_year, ic=_ic, inst_status=_inst_status)
                elif _app_start.year < self._epr_year - 1:
                    '{total:.2f} EPR due in {year} but seems to have started on {app_start}!'.format(
                        total=self.get_dues(_cms_id), app_start=_app_start, year=self._epr_year)
                if _issue is not None:
                    self.add_issue_for(_cms_id, _issue)
                    self.add_remedy(ApplicantDueRemedy(_cms_id, self._epr_year, 0, _app_start))


class IncorrectAppDueWatchdog(ApplicantDueWatchdog):
    def __init__(self, epr_year=None):
        super().__init__(epr_year=epr_year)

    def check(self):
        total_discrepancy = 0
        for _cms_id in self.applicant_cms_ids:
            _expected = self.get_expected_this_year_due(_cms_id)
            _billed = self.get_dues(_cms_id)
            if abs(_expected - _billed) > 0.01:
                self.add_issue_for(_cms_id, 'Expected due: {0:.2f}, billed : {1:.2f}, applicant since: {2}, X: {3:5.2f}, block: {4}, unblocked: {5}, blocked: {6}'.
                                   format(_expected, _billed,
                                          self.get_app_start_date(_cms_id) or '  NEVER   ',
                                          self.get_credit_predating_application(_cms_id),
                                          'Y' if self.is_blocked(cms_id=_cms_id) else 'N',
                                          self.get_unblock_date(_cms_id) or '  NEVER   ',
                                          self.get_block_date(_cms_id) or '  NEVER   '
                                          ))
                total_discrepancy += (_billed - _expected)
                self.add_remedy(ApplicantDueRemedy(_cms_id, self._epr_year, _expected, self.get_app_start_date(_cms_id)))

        if total_discrepancy != 0:
            self.add_issue('Total discrepancy: {0:.3f}'.format(total_discrepancy))