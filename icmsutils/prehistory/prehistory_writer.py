from datetime import datetime as dt
from icms_orm.cmspeople import Person, PeopleFlagsAssociation, MoData, User


class PersonAndPrehistoryWriter(object):
    """
    Takes care of changing legacy DB objects.
    """

    class Mode(object):

        @staticmethod
        def ICMS_FOR(p, date=None):
            return 'iCMSfor%d_%s' % (p.cmsId, (date or dt.now()).strftime('%d/%m/%Y'))

        @staticmethod
        def DIRECT(p, date=None):
            return '%s_%s' % (p.loginId or p.niceLoginId, (date or dt.now()).strftime('%d/%m/%Y'))

    def __init__(self, db_session, subject_person, actor_person, mode=None, date=None, mo=None, user=None):
        self._ssn = db_session
        self.subject = subject_person
        self.actor = actor_person
        self.changes = dict()
        self.flags_to_grant = set()
        self.flags_to_revoke = set()
        self.mode = mode or PersonAndPrehistoryWriter.Mode.ICMS_FOR
        self.owned_flags = {f.id for f in self.subject.flags}
        self.date = date or dt.now().date()
        self.mo_data = mo
        self.user_data = user

    @property
    def ssn(self):
        return Person.session()

    def set_new_value(self, attribute, value):
        """
        :param attribute: reference a field from Person class
        :param value: the new value
        :return:
        """
        self.changes[attribute] = value

    def grant_flag(self, flag_id):
        if flag_id in self.flags_to_revoke:
            self.flags_to_revoke.remove(flag_id)
        elif flag_id not in self.owned_flags:
            self.flags_to_grant.add(flag_id)

    def revoke_flag(self, flag_id):
        if flag_id in self.flags_to_grant:
            self.flags_to_grant.remove(flag_id)
        elif flag_id in self.owned_flags:
            self.flags_to_revoke.add(flag_id)

    def __append_history(self, change_info):
        self.subject.history.history = '\r\n'.join(
            ['%s-%s' % (self.mode(self.actor, self.date), change_info), self.subject.history.history])

    def __get_subject(self, field):
        if field.class_ == Person:
            return self.subject
        elif field.class_ == MoData:
            if not self.mo_data:
                self.mo_data = self.__retrieve_subject_from_db(field)
            return self.mo_data
        elif field.class_ == User:
            if not self.user_data:
                self.user_data = self.__retrieve_subject_from_db(field)
            return self.user_data

    def __retrieve_subject_from_db(self, field):
        return self.ssn.query(field.class_).filter(field.class_.cmsId == self.subject.cmsId).one()

    def apply_changes(self, do_commit=False):
        """
        Modifies
        :param do_commit:
        :return:
        """

        if hasattr(self, '_safety_token'):
            raise Exception('This writer object has already been used to write changes.')

        for field, new_value in self.changes.items():
            subject = self.__get_subject(field)

            self.__append_history('%s:%s' % (field.name, str(getattr(subject, field.key) or '')))
            setattr(subject, field.key, new_value)

            if field == Person.isAuthorSuspended:
                setattr(self.subject, (Person.dateAuthorSuspension if new_value else Person.dateAuthorUnsuspension).key,
                        self.date)
            elif field == Person.isAuthorBlock:
                setattr(self.subject, (Person.dateAuthorBlock if new_value else Person.dateAuthorUnblock).key,
                        self.date)

        for flag_id in self.flags_to_grant:
            new_flag = PeopleFlagsAssociation()
            (new_flag.cmsId, new_flag.flagId) = (self.subject.cmsId, flag_id)
            print( 'New flag assoc for %d: %s' % (new_flag.cmsId, new_flag.flagId) )
            self.ssn.add(new_flag)
            self.__append_history('added%s' % flag_id)

        for flag_id in self.flags_to_revoke:
            self.ssn.query(PeopleFlagsAssociation).filter(PeopleFlagsAssociation.cmsId == self.subject.cmsId).filter(
                PeopleFlagsAssociation.flagId == flag_id).delete()
            self.__append_history('removed%s' % flag_id)

        for x in [y for y in (self.subject, self.mo_data, self.user_data) if y]:
            self.ssn.add(x)

        self.ssn.add(self.subject.history)
        if do_commit:
            self.ssn.commit()
        self._safety_token = 'Writer used already'
