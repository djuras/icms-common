#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from collections import namedtuple
import time
import logging


class Cache(dict):

    ParamStore = namedtuple('ParamStore', ['args', 'kwargs'])

    def __init__(self, func, timeout=None):
        self.func = func
        self.timeout = timeout
        self.timeouts = {}

    def __call__(self, *args, **kwargs):
        key = Cache._to_hashable(args, kwargs)
        if key in self.timeouts and self.timeouts[key] < time.time():
            # reset cache and timeout!
            del self.timeouts[key]
            del self[key]
        else:
            logging.debug('Cache hit for {0}'.format(self.func.__name__))
        # the line below will trigger cache miss if the timeout had expired
        return self[key]

    def __missing__(self, key):
        args, kwargs = Cache._from_hashable(key)
        elapsed = -1 * time.time()
        result = self[key] = self.func(*args, **kwargs)
        elapsed += time.time()
        logging.info('Reloading cache for %s took %.3f seconds' % (self.func.__name__, elapsed))
        if self.timeout:
            # store timeout for params combination
            self.timeouts[key] = time.time() + self.timeout
        return result

    @staticmethod
    def expiring(timeout):
        def intermediate_decorator(func):
            return Cache(func=func, timeout=timeout)
        return intermediate_decorator

    @staticmethod
    def permanent(func):
        return Cache(func=func, timeout=None)

    @staticmethod
    def _to_hashable(args, kwargs):
        return Cache.ParamStore(args=tuple(args), kwargs=tuple(kwargs.items()))

    @staticmethod
    def _from_hashable(x):
        args = []
        kwargs = {}

        for e in x.args:
            args.append(e)
        for t in x.kwargs:
            kwargs[t[0]] = t[1]
        return args, kwargs
