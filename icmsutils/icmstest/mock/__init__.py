from .old_icms_mock_factories import *
from .new_icms_mock_factories import *

from datetime import date
mock_date_param_name = 'mock_date'

def today():
    return getattr(today, mock_date_param_name, None) or date.today()
