from .base_classes import MockFactory
from icms_orm.toolkit import AuthorshipApplicationCheck as AAC

from icms_orm.common import MO, MoStatusValues, Person, GenderValues, InstituteLeader, Affiliation, Institute, LegacyFlag
from icms_orm.common import FundingAgency, Country
from .old_icms_mock_factories import MockPersonFactory as OldIcmsMockPersonFactory

import random
from datetime import date


class MockMoFactory(MockFactory):
    _defaults = {
        MO.status: MoStatusValues.PROPOSED,
    }

    @classmethod
    def create_instance(cls, instance_overrides=None, person=None):
        _mock = cls._create_basic_mock(instance_overrides=instance_overrides, mocked_class=MO, defaults=cls._defaults)
        if person:
            _mock.set(MO.cms_id, person.get(Person.cms_id))
        return _mock


class MockNewPersonFactory(OldIcmsMockPersonFactory):

    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, Person, cls._defaults)
        for _attr, _value_fn in [
            (Person.cms_id, lambda: cls._next_cms_id()),
            (Person.hr_id, lambda: cls._next_hr_id()),
            (Person.gender, lambda: random.choice(GenderValues.values())),
            (Person.first_name, lambda: cls._pick_first_name(_mock.get(Person.gender))),
            (Person.last_name, lambda: cls._pick_last_name())
        ]:
            if not _mock.get(_attr):
                _mock.set(_attr, _value_fn())
        return _mock


class MockAffiliationFactory(MockFactory):

    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, Affiliation, cls._defaults)
        if _mock.get(Affiliation.start_date) is None:
            _mock.set(Affiliation.start_date, date.today())
        return _mock


class MockInstituteLeaderFactory(MockFactory):

    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, InstituteLeader, cls._defaults)
        if _mock.get(InstituteLeader.start_date) is None:
            _mock.set(InstituteLeader.start_date, date.today())
        return _mock


class MockInstituteFactory(MockFactory):

    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, Institute, cls._defaults)
        return _mock


class MockLegacyFlagFactory(MockFactory):
    _defaults = {LegacyFlag.remote_id: -1, LegacyFlag.start_date: date(2010, 1, 1)}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, LegacyFlag, cls._defaults)
        return _mock


class MockAuthorshipApplicationCheckFactory(MockFactory):
    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None, person=None):
        if isinstance(person, Person):
            AAC.cmsId = person.cms_id
        _mock = cls._create_basic_mock(instance_overrides, AAC, cls._defaults)
        return _mock


class MockFundingAgencyFactory(MockFactory):
    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, FundingAgency, cls._defaults)
        return _mock


class MockCountryFactory(MockFactory):
    _defaults = {}

    @classmethod
    def create_instance(cls, instance_overrides=None):
        _mock = cls._create_basic_mock(instance_overrides, Country, cls._defaults)
        return _mock
