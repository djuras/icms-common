#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from datetime import datetime, date, timedelta

# datetime format for parsing things like: Tue, 8 Nov 2016 16:12:02 CET
DATETIME_FMT_VERBOSE = '%a, %d %b %Y %H:%M:%S %Z'
# datetime format for parsing datetimes like 2017/11/18 03:37:49
DATETIME_FMT_CONCISE = '%Y/%m/%d %H:%M:%S'
# date format for parsing things like: Tue, 8 Nov 2016
DATE_FMT_VERBOSE = '%a, %d %b %Y'
# date format for parsing things like 17/01/2016
DATE_FMT_CONCISE = '%d/%m/%Y'


def parse_datetime(datetime_string, format):
    return datetime_string and datetime.strptime(datetime_string, format) or None


def parse_datetime_verbose(dt_string):
    """
    # Parses things like "Tue, 8 Nov 2016 16:12:02 CET"
    """
    return parse_datetime(dt_string, DATETIME_FMT_VERBOSE)


def parse_date_concise(date_string):
    return parse_datetime(date_string, DATE_FMT_CONCISE).date()


def datetime_to_str(
            value # type: datetime
        ,   format # type: str
        ):
    return value.strftime(format)


def datetime_to_str_verbose(value, unpad_day=False):
    result = datetime_to_str(value, DATETIME_FMT_VERBOSE)
    return result.replace(', 0', ', ') if unpad_day else result


def datetime_to_str_concise(value):
    return datetime_to_str(value, DATETIME_FMT_CONCISE)


# def date_to_str(
#             value # type: date
#         ,   format # type: str
#     ):
#     return value.strftime(format)


def date_to_str_verbose(value, unpad_day=False):
    result = datetime_to_str(value, DATE_FMT_VERBOSE)
    return result.replace(', 0', ', ') if unpad_day else result


def date_to_str_concise(value):
    return datetime_to_str(value, DATE_FMT_CONCISE)


def year_fraction_left(start_date):
    """
    Returns the year fraction between the start_date and the end of the corresponding year
    :param start_date:
    :return:
    """
    if isinstance(start_date, datetime):
        start_date = start_date.date()
    days_a_year = (date(start_date.year, 12, 31) - date(start_date.year-1, 12, 31)).days
    return 1.0 * (date(start_date.year+1, 1, 1) - start_date).days / days_a_year


def get_verbose_str_date_range_patterns(date_from, date_till):
    """
    :return: a list of SQL-compatible date patters to use when querying a DB storing dates as verbose strings
     sample use: q.filter(or_(*[CadiHistory.updateDate.like(x) for x in <FUNCTION_RESULT>]))
    """
    patterns = []
    date_from, date_till = (isinstance(x, datetime) and x.date() or x for x in [date_from, date_till])
    day_now = date_from
    while day_now <= date_till:
        if day_now.day == 1 and day_now.month == 1:
            # just grab the pattern spanning whole year
            far_end = date(day_now.year, 12, 31)
            if far_end <= date_till:
                patterns.append('%%%s%%' % day_now.strftime('%Y'))
                day_now = date(day_now.year + 1, 1, 1)
                continue
        if day_now.day == 1:
            # select whole month if possible
            next_month_no = day_now.month + 1 if day_now.month < 12 else 1
            far_end = date(next_month_no != 1 and day_now.year or day_now.year + 1, next_month_no, 1) - timedelta(days=1)
            if far_end <= date_till:
                # select the whole month
                patterns.append('%%%s%%' % day_now.strftime('%b %Y'))
                day_now = far_end + timedelta(days=1)
                continue
        # keep going day-by-day (beware of default day outputting with a leading zero - it will not parse "Oct, 8" etc.)
        patterns.append('%%%s%%' % (lambda x: x.startswith('0') and x[1:] or x)(day_now.strftime('%d %b %Y')))
        day_now = day_now + timedelta(days=1)
    return patterns


def recover_datetime(timestring, formats=(DATETIME_FMT_CONCISE, DATETIME_FMT_VERBOSE)):
    """
    Takes an input string and tries to parse it as a timestamp, using all known formats
    :param timestring: the raw input
    :param formats: formats (patterns) to use
    :return: None if unable to parse the timestamp
    """
    for pattern in formats:
        try:
            return datetime.strptime(timestring, pattern)
        except ValueError as e:
            pass
    return None
