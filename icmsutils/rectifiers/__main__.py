from .exit_date_rectifier import ExitDateRectifier
from icms_orm.common import PersonStatus
from datetime import date

if __name__ == '__main__':
    # unused import but necessary for the db connection to be initialized
    from scripts.prescript_setup import db
    from scripts.sync.helpers import TimeLinesJanitor
    janitor = TimeLinesJanitor(filter_dict={PersonStatus.cms_id: 5862})
    janitor.declutter()
    janitor.defragment()
    rectifier = ExitDateRectifier(5862, date(2018, 5, 1))
    rectifier.fix()