#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import abc


class CachedMap(object):
    def __init__(self):
        self._data = dict()

    @abc.abstractmethod
    def reload_data(self):
        raise Exception('Not implemented!')

    def map_key(self, key):
        if key not in self._data:
            self.reload_data()
        return self._data.get(key, None)

    def wipe(self):
        self._data.clear()

    def __call__(self, key):
        return self.map_key(key)
