import logging
import traceback
from . import exceptions

from icms_orm import PseudoEnum
from icms_orm.common import RequestStatusValues, RequestStepStatusValues, RequestStep, Request
from sqlalchemy.orm import joinedload, contains_eager
from icmsutils.datatypes import ActorData
from .base_classes import BaseRequestProcessor, BaseRequestExecutor


class RequestsManager(object):
    _processor_classes = {}
    _executor_classes = {}
    # request as such does not have a separate subclass per type and thus we store the names only
    _request_type_names = set()

    # THE PUBLIC API SECTION

    @classmethod
    def get_registered_request_types(cls):
        _supported = set(cls._processor_classes.keys()) & set(cls._executor_classes.keys())
        return {_t for _t in cls._request_type_names if _t in _supported}

    @classmethod
    def register_request_type_name_with_processor_and_executor(cls, request_type_name, processor_class, executor_class):
        """
        :param request_type_name: a string
        :param processor_class:
        :param executor_class:
        :return:
        """
        if request_type_name in cls._request_type_names:
            raise exceptions.RequestProcessingException('%s request type name already registered!' % request_type_name)
        if processor_class is None or processor_class == BaseRequestProcessor or not issubclass(processor_class, BaseRequestProcessor):
            raise exceptions.IllegalRequestProcessorClassException(processor_class)
        if executor_class is None or not issubclass(executor_class, BaseRequestExecutor):
            raise exceptions.IllegalRequestExecutorClassException(executor_class)

        cls._request_type_names.add(request_type_name)
        cls._processor_classes[request_type_name] = processor_class
        cls._executor_classes[request_type_name] = executor_class

    @classmethod
    def create_request(cls, request_type_name, actor_data, remarks=None, execution_date=None, **request_processing_data):
        logging.debug('Creating request stub.')
        if not request_type_name in cls.get_registered_request_types():
            raise exceptions.UnsupportedRequestTypeException(requested_type=request_type_name)

        assert isinstance(actor_data, ActorData)
        _request = Request.from_ia_dict({
            Request.creator_cms_id: actor_data.cms_id,
            Request.processing_data: request_processing_data,
            Request.type: request_type_name,
            Request.remarks: remarks,
            Request.status: RequestStatusValues.PENDING_APPROVAL,
            Request.scheduled_execution_date: execution_date,
        })
        # No need to forward the remarks as with creation they are stored in the request itself.
        # Request will be committed to DB in _handle_step_action
        cls._handle_step_action(request=_request, action=BaseRequestProcessor.Action.CREATE, actor_data=actor_data,
                                remarks=None)
        return _request

    @classmethod
    def approve_step(cls, request_id, actor_data, step_id=None, remarks=None):
        return cls._handle_step_action(request=cls.get_request_data(request_id), actor_data=actor_data,
                                       step_id=step_id, remarks=remarks, action=BaseRequestProcessor.Action.APPROVE)

    @classmethod
    def reject_step(cls, request_id, actor_data, step_id=None, remarks=None):
        return cls._handle_step_action(request=cls.get_request_data(request_id), actor_data=actor_data,
                                       step_id=step_id, remarks=remarks, action=BaseRequestProcessor.Action.REJECT)

    @classmethod
    def get_request_steps_pending_action(cls, actor_data=None):
        """
        :param actor_data:
        :return: Iterable of RequestStep objects
        """
        _all_pending_steps = Request.session().query(RequestStep).\
            join(Request, RequestStep.request_id == RequestStep.request_id).\
            filter(Request.status == RequestStatusValues.PENDING_APPROVAL).\
            filter(RequestStep.status == RequestStepStatusValues.PENDING).\
            options(joinedload(RequestStep.request)).all()
        if not actor_data:
            return _all_pending_steps
        else:
            _steps_that_can_be_acted_upon = []
            for _step in _all_pending_steps:
                _p = cls._get_request_processor_instance(request_data=_step.get(RequestStep.request))
                if _p.can_act(actor_data=actor_data, step=_step):
                    _steps_that_can_be_acted_upon.append(_step)
            return _steps_that_can_be_acted_upon

    @classmethod
    def get_request_data(cls, request_id):
        """
        :param request_id: The DB ID of the request.
        :return: Database Request object along with its corresponding steps
        """
        _ssn = Request.session()
        return _ssn.query(Request).filter(Request.id == request_id).options(joinedload(Request.steps)).one()

    @classmethod
    def get_requests_by_remarks(cls, remarks):
        return Request.session().query(Request).filter(Request.remarks.like('%{remarks}%'.format(remarks=remarks))).all()

    # THE HEAVY LIFTING SECTION OF PRIVATE AND PROTECTED METHODS

    @classmethod
    def _handle_step_action(cls, request, action, actor_data, step_id=None, remarks=None):
        """
        The core method of request processing, linking it all together.
        Every action, including request creation should pass through this method to assure uniform handling.
        """
        try:
            assert isinstance(actor_data, ActorData)
            _type = request.get(Request.type)
            _processor = cls._get_request_processor_instance(request_data=request)
            assert isinstance(_processor, BaseRequestProcessor)
            _processor.handle_step(action=action, actor_data=actor_data, step_id=step_id, remarks=remarks)
            # todo: test that all the associated steps are preserved correctly in the database
            Request.session().add(request)
            try:
                cls._execute_request_if_cleared(request)
            except Exception:
                raise
            finally:
                Request.session().commit()
        except Exception as e:
            logging.error('Error performing %s action on a request!' % (action,))
            logging.error(traceback.format_exc())
            Request.session().rollback()
            raise

    @classmethod
    def _get_request_processor_instance(cls, request_data):
        _cls = cls._get_request_processor_class(request_data.get(Request.type))
        assert issubclass(_cls, BaseRequestProcessor)
        return _cls(request=request_data)

    @classmethod
    def _get_request_processor_class(cls, request_type_name):
        _cls = cls._processor_classes.get(request_type_name)
        assert issubclass(_cls, BaseRequestProcessor)
        return _cls

    @classmethod
    def _get_request_executor_class(cls, request_type_name):
        """
        :type request_type_name: str
        """
        _cls = cls._executor_classes.get(request_type_name)
        assert issubclass(_cls, BaseRequestExecutor)
        return _cls

    @classmethod
    def _get_request_executor_instance(cls, request_data):
        _cls = cls._get_request_executor_class(request_type_name=request_data.get(Request.type))
        assert issubclass(_cls, BaseRequestExecutor)
        return _cls(**request_data.get(Request.processing_data))

    @classmethod
    def _is_cleared_for_execution(cls, request_data):
        return request_data.get(Request.status) == RequestStatusValues.READY_FOR_EXECUTION

    @classmethod
    def _execute_request_if_cleared(cls, request_data):
        if cls._is_cleared_for_execution(request_data):
            try:
                _executor = cls._get_request_executor_instance(request_data)
                _executor.execute()
                request_data.set(Request.status, RequestStatusValues.EXECUTED)
            except Exception as e:
                logging.error('Request execution failed for request ID {request_id}'.format(
                    request_id=request_data.get(Request.id)))
                logging.error(traceback.format_exc())
                request_data.set(Request.status, RequestStatusValues.FAILED_TO_EXECUTE)
                raise
