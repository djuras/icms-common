from icms_orm.toolkit import VoteDelegation, VotingMerger, VotingMergerMember, Voting
from icms_orm.common import Person, Institute, Affiliation, MO, InstituteLeader, InstituteStatus
from icms_orm.toolkit import VoteDelegation
from icmsutils.businesslogic.mo import PhdCountModel
import sqlalchemy as sa
from datetime import timedelta, date

class OldVotingListModel(object):

    def __init__(self, voting_date=None, is_election=False, voting_code=None):
        ssn = flask.g.db.session
        voting = ssn.query(Voting).filter(Voting.code == voting_code).one() if voting_code else None
        voting_date = voting_date or voting.start_time
        is_election = is_election or voting.type and voting.type.lower() == 'election'

        self.voting_code = voting_code
        self.voting_date = voting_date

        # delegate or the cbi
        self._voting_cms_ids = set()
        # voting entities' nominal representatives, no delegations!
        self._entitled_cms_ids = set()
        # phds_now = mo.get_phds_per_inst(ssn, voting_date.year)
        count_year = PhdCountModel(db_session=ssn, year=voting_date.year, include_free=False)
        count_past = PhdCountModel(db_session=ssn, year=voting_date.year - 1)

        _permanent_proxies_cond = lambda is_election: (VoteDelegation.is_long_term==True) if not is_election else False

        from sqlalchemy.orm import aliased

        Delegate = aliased(Person)

        q_delegations = ssn.query(VoteDelegation, Delegate, Person). \
            join(Delegate, Delegate.cms_id == VoteDelegation.cms_id_from). \
            join(Person, Person.cms_id == VoteDelegation.cms_id_to). \
            filter(VoteDelegation.status == Status.ACTIVE).\
            filter(or_(_permanent_proxies_cond(is_election), VoteDelegation.voting_code == voting_code))

        self._delegations = {}
        self._inst_specific_delegations = {}
        self._delegates = {}
        self._delegating = {}

        self._init_delegation_data(q_delegations.all())

        table_data = Table.get_empty()
        table_data.keys = ['Country', 'Institute', 'Voting Person', 'Remarks']

        insufficient_data = {}

        insts_in_count = 0
        countries_in_count = 0

        institutes_and_cbis = ssn.query(Institute, IcmsPerson).join(IcmsPerson, Institute.cbiCmsId == IcmsPerson.cmsId)\
            .filter(Institute.dateCmsIn < voting_date - timedelta(365)).order_by(Institute.country, Institute.code).all()

        institutes = []
        all_cbis = {}

        for institute, cbi in institutes_and_cbis:
            institutes.append(institute)
            # hack to make the old entities look like the new ones in this context!
            all_cbis[institute.code] = VotingListModel.__rig_old_icms_person_entity(cbi)


        voting_by_country = {}
        # store them so that they are available for elections
        cbis_by_country = {}

        # get active mergers
        mergers_by_country = VotingMerger.query.filter(and_(
            or_(VotingMerger.valid_from == None, VotingMerger.valid_from <= voting_date),
            or_(VotingMerger.valid_till == None, VotingMerger.valid_till >= voting_date))).all()

        merger_reps = IcmsPerson.query.filter(IcmsPerson.cmsId.in_([x.representative_cms_id for x in mergers_by_country])).all()
        merger_reps = {p.cmsId: VotingListModel.__rig_old_icms_person_entity(p) for p in merger_reps}

        mergers_by_country = {m.country: [m2 for m2 in mergers_by_country if m2.country == m.country] for m in mergers_by_country}
        # here we'll store info about potential mergees - PHDs are considered spare only if there are not enough of
        # them to earn independent voting right for the institute
        spare_phd_counts = {}

        logging.debug('Mergers: %s' % str(mergers_by_country))
        logging.debug('Found %d active mergers' % len(mergers_by_country))

        for inst in institutes:
            past_phds = count_past[inst.code]
            current_phds = count_year[inst.code]
            # skipping the institutes that have no PHDs now and cannot vote thanks to last year's PHDs
            if current_phds == 0 and past_phds < 3:
                continue
            country = inst.country if inst.code != 'CERN' else 'CERN'
            if country not in voting_by_country.keys():
                voting_by_country[country] = []
            if current_phds >= 3:
                voting_by_country[country].append(inst)
            else:
                if any([x >= 3 for x in (past_phds, current_phds)]):
                    voting_by_country[country].append(inst)
                # no voting insts from that country so far...
                else:
                    insufficient_data[inst.code] = [inst.country, inst.code, inst.dateCmsIn, current_phds, past_phds]

                    if not voting_by_country[country]:
                        if country not in cbis_by_country:
                            cbis_by_country[country] = set()
                        cbis_by_country[country].add(all_cbis.get(inst.code))
                spare_phd_counts[inst.code] = current_phds

        for country in voting_by_country:
            if voting_by_country[country]:
                for inst in voting_by_country[country]:
                    voter = self._get_delegate_for(inst.cbiCmsId, inst.code) or all_cbis.get(inst.code)
                    self._add_voting_person(voter)
                    self._add_entitled_person(all_cbis.get(inst.code))
                    table_data.vals.append({
                        table_data.keys[0]: country,
                        table_data.keys[1]: inst.code,
                        table_data.keys[2]: VotingListModel.format_cbi(voter),
                        table_data.keys[3]: self._get_delegation_string(inst.cbiCmsId, inst.code)
                    })
                    insts_in_count += 1
            if is_election:
                if country in mergers_by_country:
                    for merger in mergers_by_country[country]:
                        total_phds = sum([spare_phd_counts.get(x.inst_code, 0) for x in merger.members])
                        logging.debug('Merger %d has %d PHDs' % (merger.id, total_phds))
                        if total_phds >= 3:
                            voter = self._get_delegate_for(merger.representative_cms_id) or merger_reps[merger.representative_cms_id]
                            self._add_voting_person(voter)
                            self._entitled_cms_ids.add(merger.representative_cms_id)

                            table_data.vals.append({
                                table_data.keys[0]: country,
                                table_data.keys[1]: ' & '.join(
                                    [x.inst_code for x in merger.members]) + ' [%d PHDs]' % total_phds,
                                table_data.keys[2]: VotingListModel.format_cbi(voter),
                                table_data.keys[3]: self._get_delegation_string(merger.representative_cms_id)
                            })
                            insts_in_count += 1
                            merger_member_codes = [m.inst_code for m in merger.members]
                            for code in merger_member_codes:
                                insufficient_data.pop(code)

                        else:
                            logging.warn('Merger %d does not meet the minimum PHDs count requirement.' % merger.id)
                # there are no institutes from that country as of yet, are there any mergers?
                # we use mergers table to indicate which CBI will vote for the country - that's what an empty merger is
                if not voting_by_country[country]:
                    merger = None
                    if country in mergers_by_country:
                        for m in mergers_by_country[country]:
                            if not m.members:
                                # stop conditon: national merger has no member institutes
                                merger = m
                                break

                    cbis = sorted(cbis_by_country.get(country, {}) if merger is None else [merger_reps[merger.representative_cms_id]])

                    voters = []
                    for cbi in cbis:
                        self._add_entitled_person(cbi)
                        voters.append(self._get_delegate_for(cbi.cmsId) or cbi)
                        self._add_voting_person(voters[-1])

                    table_data.vals.append({
                        table_data.keys[0]: country,
                        table_data.keys[1]: '',
                        table_data.keys[2]: ' / '.join([VotingListModel.format_cbi(x) for x in voters]),
                        table_data.keys[3]: len(cbis) == 1 and self._get_delegation_string(cbis[0].cms_id) or ''
                    })
                    # remove country's institutes from potential mergees
                    insufficient_data = {k: v for k, v in insufficient_data.items() if not country in v}
                    countries_in_count += 1

        self.insufficient = Table.get_empty(keys=['Country', 'Institute', 'Member Since',
                'PHDs in %d' % (voting_date.year), 'PHDs in %d' % (voting_date.year - 1)])
        for row in insufficient_data.values():
            self.insufficient.append_values_from_db_row(row)

        self.table_data = table_data
        self.summary = Summary(insts_in_count=insts_in_count, countries_in_count=countries_in_count)
        # a quick column reorder:
        self.table_data.keys = ['Voting Person', 'Country', 'Institute', 'Remarks']
        self._voting_by_country = voting_by_country

    @staticmethod
    def format_cbi(p):
        return '%s, %s [CMS ID %d]' % (p.last_name, p.first_name, p.cms_id)

    @staticmethod
    def get_instance(voting_date=None, is_election=False, voting_code=None):
        return VotingListModel(voting_date, is_election, voting_code)

    def is_voting(self, cms_id):
        """
        Tells if a specified person is to vote (including as a delegate)
        :param cms_id: CMS ID of a person to ask about
        :return: True if voting, either by delegation or by virtue of being a CBI, merger or country representative
        """
        return cms_id in self._voting_cms_ids

    def get_voting_inst_codes(self):
        codes = set()
        for country, insts in self._voting_by_country.items():
            for inst in insts:
                codes.add(inst.code)
        return sorted(codes)

    def get_voting_cms_ids(self):
        return self._voting_cms_ids

    def get_entitled_cms_ids(self):
        """
        Ignores delegations
        """
        return self._entitled_cms_ids

    def is_entitled(self, cms_id):
        return cms_id in self._entitled_cms_ids

    def _get_delegation_for(self, cms_id, specific_inst_code=None):
        """
        Resolves the issue with multiple institutes being led by a single person.
        The "specific_inst_code" column will be examined and an empty value will be considered an all-matching wildcard,
        while any specific value will be checked for equality with provided inst_code.
        :param cms_id:
        :param specific_inst_code:
        :return:
        """
        return self._delegations.get(cms_id, self._inst_specific_delegations.get((cms_id, specific_inst_code), None))

    def _init_delegation_data(self, rows):
        for delegation, person_from, person_to in rows:
            if getattr(delegation, VoteDelegation.specific_inst_code.key) is None:
                self._delegations[person_from.cms_id] = delegation
            else:
                self._inst_specific_delegations[(person_from.cms_id, delegation.specific_inst_code)] = delegation

            self._delegating[person_from.cms_id] = person_from
            self._delegates[person_to.cms_id] = person_to

    def _get_delegate_for(self, cms_id, specific_inst_code=None):
        delegation = self._get_delegation_for(cms_id, specific_inst_code)
        return delegation and self._delegates.get(delegation.cms_id_to) or None

    def _get_delegation_string(self, cbi_cms_id, inst_code=None):
        # 'Delegated by %s' % (format_cbi(delegating[delegations_by_delegate_id_and_inst_code[(x, y)].cms_id_from]))
        delegation = self._get_delegation_for(cbi_cms_id, inst_code)
        return delegation and 'Delegated by %s' % VotingListModel.format_cbi(self._delegating.get(delegation.cms_id_from)) or ''

    def _add_voting_person(self, p):
        self._voting_cms_ids.add(p.cms_id)

    def _add_entitled_person(self, p):
        self._entitled_cms_ids.add(p.cms_id)

    @staticmethod
    def __rig_old_icms_person_entity(e):
        return add_field_aliases(e, {'cmsId': 'cms_id', 'lastName': 'last_name', 'firstName': 'first_name'})


class VotingEntity(object):

    class Type(object):
        INSTITUTE = 'INSTITUTE'
        COUNTRY = 'COUNTRY'
        MERGER = 'MERGER'

    def __init__(self, type, code, vote_caster, vote_owner):
        """

        :param type:
        :param code:
        :param vote_caster: Person
        :param vote_owner: Person, if different from vote owner then probably some delegation has been involved
        """
        self.type = type
        self.code = code
        self.vote_caster = vote_caster
        self.vote_owner = vote_owner


class VotingListModel(object):

    def __init__(self, db_session, voting_or_code, scheduled_date=None, is_election=None):
        self._voting_entities = None
        if isinstance(voting_or_code, Voting):
            scheduled_date = voting_or_code.get(Voting.start_time).date()
            is_election = voting_or_code.get(Voting.type) == Voting.Type.ELECTION

            # to get the list of institutes
            # select count(*), inst_code, year from mo where year=2018 and end_date is null and is_free=false group by inst_code, year;
            # from there we can pick those having above 3

        sq_mo_count = db_session.query(MO.year.label('year'), MO.inst_code.label('inst_code'), sa.func.count(MO.cms_id).label('n')).filter(sa.and_(
            MO.end_date == None, MO.is_free == False, MO.year.in_([scheduled_date.year, scheduled_date.year - 1])
        )).group_by(MO.year, MO.inst_code).subquery()

        cutoff_date = date(scheduled_date.year - 1, scheduled_date.month, scheduled_date.day)

        sq_inst = db_session.query(Institute.code.label('inst_code')) \
            .join(InstituteStatus, sa.and_(Institute.code == InstituteStatus.code, InstituteStatus.end_date == None)) \
            .filter(InstituteStatus.status == 'Yes') \
            .filter(sa.or_(InstituteStatus.start_date <= cutoff_date, Institute.official_joining_date <= cutoff_date)) \
            .subquery()

        # todo: in the first iteration, let's limit ourselves to answering the simple question of which insts can vote
        data = db_session.query(sq_mo_count.c.inst_code, Person).join(sq_inst, sq_inst.c.inst_code == sq_mo_count.c.inst_code) \
            .join(InstituteLeader,
                  sa.and_(InstituteLeader.inst_code == sq_mo_count.c.inst_code, InstituteLeader.end_date == None),
                  isouter=True) \
            .join(Person, sa.and_(InstituteLeader.cms_id == Person.cms_id), isouter=True) \
            .filter(sq_mo_count.c.n > 2).all()
        self._voting_entities = {r[0]: VotingEntity(type=VotingEntity.Type.INSTITUTE, code=r[0], vote_caster=r[1], vote_owner=r[1]) for r in data}

    def get_voting_insts(self):
        return self._voting_entities.keys()

    def who_votes_for_inst(self, inst_code):
        return self._voting_entities.get(inst_code).vote_caster

    def get_voting_entities(self):
        return self._voting_entities.values()


