from datetime import date, datetime
from icms_orm.cmsanalysis import CDSLog
from icmsutils.timedate import recover_datetime
from utils import make_histograms_for_periods
import sqlalchemy as sa
import logging


def response_time(ssn, cutoff_date=date(2016, 1, 1), months_per_partition=6, bucket_bounds=[300, 30 * 60, 5 * 3600, 24 * 3600]):
    cutoff = datetime(cutoff_date.year, cutoff_date.month, cutoff_date.day, 0, 0)
    entries = ssn.query(CDSLog.action, CDSLog.code, CDSLog.update_time).\
        filter(
        sa.or_(CDSLog.action == 'cadiNOTIFY', sa.and_(CDSLog.action.in_(['SUBMIT', 'NOCOMMENTS']), CDSLog.status == 'start')
    )).order_by(CDSLog.code, CDSLog.id).all()

    logging.debug('Found %d entries' % len(entries))

    open = {}

    all_samples = []

    for action, code, ts in entries:
        ts = recover_datetime(ts)
        if action in {'SUBMIT', 'NOCOMMENTS'}:
            if code not in open:
                open[code] = []
            open[code].append(ts)
        elif action == 'cadiNOTIFY':
            if code in open and open[code]:
                started = open[code].pop(0)
                if started and started >= cutoff:
                    all_samples.append((code, started, ts))
            else:
                logging.debug('Weird? %s not in open when NOTIFY comes' % code)

    for code in open:
        if open[code]:
            logging.debug('%s has %d unclosed entries!' % (code, len(open[code])))

    logging.debug('Collected %d samples' % len(all_samples))
    return make_histograms_for_periods(all_samples, cutoff=cutoff, months_per_partition=months_per_partition, bucket_bounds=bucket_bounds)