from icms_orm.cmspeople import Person as OldPerson
from icms_orm.common import Person as NewPerson
from icmsutils.exceptions import IcmsException


class Person(object):
    def __init__(self, cms_id, first_name, last_name, inst_code=None):
        self._cms_id = cms_id
        self._first_name = first_name
        self._last_name = last_name
        self._inst_code = inst_code

    @property
    def cms_id(self):
        return self._cms_id

    @property
    def first_name(self):
        return self._first_name

    @property
    def last_name(self):
        return self._last_name

    @property
    def inst_code(self):
        return self._inst_code

    @classmethod
    def from_data_object(cls, data_object, overrides=None):
        overrides = overrides or {}
        params = {}
        if isinstance(data_object, OldPerson):
            params = dict(cms_id=data_object.cmsId, first_name=data_object.firstName, last_name=data_object.lastName,
                          inst_code=data_object.instCode)
        elif isinstance(data_object, NewPerson):
            params = dict(cms_id=data_object.cms_id, first_name=data_object.first_name, last_name=data_object.last_name)
        else:
            raise IcmsException('Cannot translate {0} into a Person object'.format(data_object))
        params.update(overrides)
        return Person(**params)

    @property
    def label(self):
        return '{last_name}, {first_name} [CMS ID {cms_id}]'.format(first_name=self.first_name,
                                                                    last_name=self.last_name, cms_id=self.cms_id)

    def __hash__(self) -> int:
        return self.cms_id.__hash__()

    def __eq__(self, other):
        return isinstance(other, Person) and self.cms_id == other.cms_id

