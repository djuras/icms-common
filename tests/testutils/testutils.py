import logging
import traceback

import pytest
from sqlalchemy import MetaData
from sqlalchemy import create_engine

from icmsutils.icmstest import mock as mockables
from icmscommon import ConfigGovernor as ConGov, DbGovernor
from icms_orm import cms_common_bind_key, toolkit_bind_key, cms_people_bind_key, epr_bind_key, cadi_bind_key, DatabaseViewMixin
from icmsutils.dbutils import pg_parse_login_pass_host_db as _extract_db_login_pass_host_db
import subprocess

db = DbGovernor.get_db_manager()

#-ap
# the following DBs need to exist in the mysql service:
#
# create database test_cmspeople;
# create database test_cmsanalysis;
# create database test_metadata;
#
# and the user icms_test needs to be able to login on 'localhost' (not just 127.0.0.1 or ::1)
#
#-ap end


def drop_all_enums(conn_str):
    """
    create_all and drop_all leaves some mess behind or attempts to drop the enums without cascade before dropping
    affected tables...
    """
    (_user, _pass, _host, _db) = _extract_db_login_pass_host_db(conn_str=conn_str)
    enum_names = []

    s, o = run_psql_command(password=_pass, user=_user, db=_db, host=_host, command='\dT')

    lines = o.split('\n')
    try:
        for line in lines[3:]:
            try:
                tokens = line.split('|')
                if len(tokens) > 1:
                    enum_names.append('%s.%s' % (tokens[0].strip(), tokens[1].strip()))
            except Exception as e:
                logging.warning('You can probably ignore this problem: {0}'.format(e))
                logging.warning(traceback.format_exc())
    except Exception as e:
        logging.error('I tried but {0}'.format(e))

    db_commands = ['DROP TYPE %s cascade;' % enum_name for enum_name in enum_names]
    for command in db_commands:
        run_psql_command(password=_pass, user=_user, host=_host, db=_db, command=command)
    db.session.commit()


def recreate_db_by_bind(bind, call_when_destroyed=(lambda: None)):
    db.session.rollback()
    connection_string = db.config['SQLALCHEMY_BINDS'][bind]
    print('DB under bind %s is about to be obliterated' % bind)

    """
    SQLAlchemy tends to create all MySQL enums in the postgres DB which is problematic when it comes
    to dropping and creating that DB a couple of times in the row - it might get created by one user
    only to be dropped by another - which is illegal and causes crashes
    """
    # if 'mysql://' not in connection_string:
        # db_engine = create_engine(connection_string)
        # meta = MetaData()
        # meta.reflect(bind=db_engine)
    logging.info('Dropping the DB by bind: {bind}'.format(bind=bind))
    if 'postgresql' in connection_string:
        drop_all_enums(conn_str=connection_string)
        drop_postgres_tables_with_cascade(bind)
    db.drop_all(bind=bind)
    # give an opportunity for some injected code to execute
    db.session.commit()
    if callable(call_when_destroyed):
        call_when_destroyed()
    # as it turns out, some tables survive the above call (especially the _version ones)
    # it somehow fails inside SQL Alchemy's code for mysql...
    if 'mysql://' not in connection_string:
        db_engine = create_engine(connection_string)
        meta = MetaData()
        meta.reflect(bind=db_engine)
        for table in reversed(meta.sorted_tables):
            db_engine.execute(table.delete())
        db_engine.dispose()
    db.create_all(bind=bind)
    DatabaseViewMixin.create_views(db.session, bind=bind)
    print('DB under bind %s was reborn' % bind)
    if bind == cms_common_bind_key():
        (_user, _pass, _host, _daba) = _extract_db_login_pass_host_db(connection_string)
        run_psql_command('GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to icms_test_reader;',
                         _user, _pass, _daba, _host)


def recreate_people_db():
    return recreate_db_by_bind(bind=cms_people_bind_key())


@pytest.fixture(name='recreate_people_db')
def recreate_people_db_fixture():
   return recreate_people_db()


def recreate_old_cadi_db():
    recreate_db_by_bind(bind=cadi_bind_key())

@pytest.fixture(name='recreate_old_cadi_db')
def recreate_old_cadi_db_fixture():
    recreate_db_by_bind(bind=cadi_bind_key())


def recreate_common_db():
    recreate_db_by_bind(bind=cms_common_bind_key())

@pytest.fixture(name='recreate_common_db')
def recreate_common_db_fixture():
    recreate_common_db()

def recreate_epr_db():
    recreate_db_by_bind(bind=epr_bind_key())

@pytest.fixture(name='recreate_epr_db')
def recreate_epr_db_fixture():
    recreate_epr_db()

def recreate_toolkit_db(call_when_destroyed=lambda: None):
    recreate_db_by_bind(bind=toolkit_bind_key(), call_when_destroyed=call_when_destroyed)

@pytest.fixture(name='recreate_toolkit_db')
def recreate_toolkit_db_fixture():
    recreate_toolkit_db()

def drop_postgres_tables_with_cascade(bind):
    connection_string = db.config['SQLALCHEMY_BINDS'][bind]
    (_user, _pass, _host, _daba) = _extract_db_login_pass_host_db(connection_string)
    for table_name in db.get_tables_for_bind(bind):
        _q = 'DROP TABLE IF EXISTS {table_name} CASCADE;'.format(table_name=table_name)
        run_psql_command(command=_q, user=_user, password=_pass, host=_host, db=_daba)

@pytest.fixture(name='drop_postgres_tables_with_cascade')
def drop_postgres_tables_with_cascade_fixture(bind):
    return drop_postgres_tables_with_cascade

@pytest.fixture
def init_postgres_db_rights():
    creds = {_db: _extract_db_login_pass_host_db(ConGov.get_db_string(_db)) for _db in ['cms_common', 'epr', 'toolkit']}
    schemas = {_db: ConGov.get_db_string('%s_schema_name' % _db) for _db in ['cms_common', 'epr', 'toolkit']}

    logging.debug(creds)
    logging.debug(schemas)

    _commands = '''  CREATE ROLE icms_test_reader;
                     GRANT CONNECT ON DATABASE {pg_db} to icms_test_reader;
                     GRANT USAGE ON SCHEMA {epr_schema} to icms_test_reader;
                     GRANT USAGE ON SCHEMA {toolkit_schema} to icms_test_reader;
                     GRANT USAGE ON SCHEMA public to icms_test_reader;
                     GRANT SELECT ON ALL TABLES IN SCHEMA {epr_schema} to icms_test_reader;
                     GRANT SELECT ON ALL TABLES IN SCHEMA {toolkit_schema} to icms_test_reader;
                     GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to icms_test_reader;
                     GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public to icms_test_reader;
                     GRANT icms_test_reader to {epr_user}, {toolkit_user}, {icms_user};'''.format(
        pg_db=creds['toolkit'][-1], epr_schema=schemas['epr'], toolkit_schema=schemas['toolkit'],
        epr_user=creds['epr'][0], toolkit_user=creds['toolkit'][0], icms_user=creds['cms_common'][0]
    )
    _user, _pass = ConGov.get_db_owner_user_pass()
    for line in [_l.strip() for _l in _commands.splitlines()]:
        run_psql_command(command=line, password=_pass, user=_user, db=creds['cms_common'][-1],
                         host=creds['cms_common'][-2])


@pytest.fixture
def reload_people_db():
    pass


@pytest.fixture
def reload_toolkit_db():
    db.session.rollback()
    # can't go this way - Enums from public schema are associated with the same MetaData instance so Alchemy tries to
    # drop them too (although none of the dropped tables references them)
    # db.drop_all(bind=toolkit_bind_key())
    # db.create_all(bind=toolkit_bind_key())



def set_db_access_rights():
    (_user, _pass, _host, _db) = _extract_db_login_pass_host_db(ConGov.get_epr_db_string())
    _db_owner_user, _db_owner_pass = ConGov.get_db_owner_user_pass()

    for cmd in [
        'ALTER SCHEMA epr OWNER TO %s' % _user,
        'ALTER SCHEMA toolkit OWNER TO %s' % _user,
        'REASSIGN OWNED by epr to %s' % _user,
        'GRANT USAGE ON SCHEMA epr to icms_test_reader',
        'GRANT USAGE ON SCHEMA toolkit to icms_test_reader',
        'GRANT SELECT ON ALL TABLES IN SCHEMA epr to icms_test_reader',
        'GRANT SELECT ON ALL TABLES IN SCHEMA toolkit to icms_test_reader',
    ]:
        run_psql_command(command=cmd, user=_db_owner_user, password=_db_owner_pass, db=_db)


def close_enough(x, y, abs_epsilon, rel_epsilon=None):
    diff = abs(x - y)
    if diff < abs_epsilon:
        return True
    elif rel_epsilon is not None and 2 * diff / (x + y) < rel_epsilon:
        return True
    return False


def assert_attr_val(ent_class, filters_dict, assertions_dict, msg='', db_fn=lambda: db, lambdas_list=None):
    """
    example: assert_attr_value(Person, {Person.cmsId: 9981}, {Person.lastName: 'Bawej'},
    :param ent_class: mapper class so that the correct DB table can be queried
    :param filters_dict: a dict of {attr: value} format to pick the right DB entry
    :param assertions_dict: format like above, but these will be checked against the entry retrieved from the DB
    :param msg: something to print in case the assertion fails
    :param db_fn: usually nothing needs to be provided here as most likely the db object will have been imported by
     the time this method is called and therefore the default will do just fine resolving the variable
    :param lambdas: lambdas to be applied on a query in a q=fn(q) fashion
    :return: nothing, hopefully
    """
    records = db_fn().session.query(ent_class)
    for key, val in filters_dict.items():
        records = records.filter(key == val)
    for _lambda in lambdas_list or []:
        records = _lambda(records)
    records = records.all()
    query_params_string = ' and '.join(['%s=%s' % (key.key, str(val)) for key, val in filters_dict.items()])
    for record in records:
        for attr, expected in assertions_dict.items():
            actual = getattr(record, attr.key)
            assert actual == expected, msg or 'Expected value of %s was %s but found %s for query params %s' % \
                                              (attr.key, str(expected), str(actual), query_params_string)
    if not records:
        raise AssertionError('No records of type %s found for query params: %s' % (str(ent_class.__name__), query_params_string))


def count_in_db(ent_class, filters_dict, lambdas_list=None):
    q = db.session.query(ent_class)
    for _key, _val in filters_dict.items():
        q = q.filter(_key == _val)
    for _lambda in lambdas_list or []:
        q = _lambda(q)
    return q.count()


def set_mock_date(new_date):
    setattr(mockables.today, mockables.mock_date_param_name, new_date)


def run_psql_command(command, user, password, db, host='127.0.0.1', log_fn=logging.info):
    cmd = 'PGPASSWORD={pwd} psql -U {user} -d {db} -h {host} -c "{query}"'.format(pwd=password, user=user, db=db, host=host, query=command)
    # (s, o) = commands.getstatusoutput(cmd)
    s = ''
    o = ''
    try:
        result = subprocess.run(cmd, check=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        s = result.returncode
        o = result.stdout.decode('utf-8')
    except subprocess.CalledProcessError as e:
        logging.error('running cmd "%s" failed: %s' % (command, str(e)) )
        pass

    log_fn('PSQL command: {cmd} / status: {status}'.format(cmd=command, status=s))
    return s, o


def cartesian(a, b):
    (a, b) = (list(_e() if callable(_e) else _e) for _e in (a, b))
    return zip(sorted(a * len(b)), b * len(a))
