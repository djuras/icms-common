from scripts.prescript_setup import db
import pytest
from scripts.sync.helpers import ForwardSyncManager, SyncRunInfo
from scripts.sync.agents import InstitutesSyncAgent, PeopleSyncAgent, AffiliationsSyncAgent
from tests.testutils import recreate_common_db, recreate_people_db
from icms_orm.common import ApplicationAsset
from datetime import datetime
from time import sleep

@pytest.fixture()
def setup():
    # we can run the tests here against empty databases
    recreate_people_db()
    recreate_common_db()


def test_sync_run_times_are_stored(setup):

    first_start_time = datetime.now().replace(microsecond=0)
    # the datetime -> timestamp -> datetime conversion roundtrip erases the sub-second part of the datetime.
    sleep(0.5)
    agent_classes = [InstitutesSyncAgent]
    assert db.session.query(ApplicationAsset).count() == 0

    ForwardSyncManager.launch_sync(agent_classes_override=agent_classes)
    first_finish_time = datetime.now().replace(microsecond=0)

    sync_info = SyncRunInfo.get_last_run_info_from_db()

    assert db.session.query(ApplicationAsset).count() == 1
    assert first_start_time <= sync_info.get_timestamp(InstitutesSyncAgent) <= first_finish_time

    sleep(3)
    # FIRST SYNC DONE, NOW LET'S ADD PEOPLE
    agent_classes.append(PeopleSyncAgent)

    second_start_time = datetime.now().replace(microsecond=0)
    sleep(1)
    ForwardSyncManager.launch_sync(agent_classes_override=agent_classes)
    second_finish_time = datetime.now().replace(microsecond=0)
    assert db.session.query(ApplicationAsset).count() == 1
    sync_info = SyncRunInfo.get_last_run_info_from_db()
    assert second_start_time <= sync_info.get_timestamp(InstitutesSyncAgent) <= second_finish_time
    assert second_start_time <= sync_info.get_timestamp(PeopleSyncAgent) <= second_finish_time
