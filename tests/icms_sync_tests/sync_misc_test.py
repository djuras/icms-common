from scripts.sync.agents import CountriesSyncAgent
from .fixtures import set_mock_date
from scripts import mockables
from datetime import date
import pytest

@pytest.mark.parametrize('country_name',
                         ['Armenia', 'Australia', 'Austria', 'Belarus', 'Belgium', 'Brazil', 'Bulgaria', 'Canada',
                          'China', 'Colombia', 'Croatia', 'Cyprus', 'Czech Republic', 'Ecuador', 'Egypt', 'Estonia',
                          'Finland', 'France', 'Georgia', 'Germany', 'Greece', 'Hungary', 'India', 'Iran', 'Ireland',
                          'Italy', 'Japan', 'Korea', 'Latvia', 'Lebanon', 'Lithuania', 'Malaysia', 'Mexico',
                          'Montenegro', 'Netherlands', 'New Zealand', 'Norway', 'Oman', 'Pakistan', 'Philippines',
                          'Poland', 'Portugal', 'Qatar', 'Romania', 'Russia', 'Saudi Arabia', 'Serbia', 'Singapore',
                          'Slovak Republic', 'Spain', 'Sri Lanka', 'Switzerland', 'Taiwan', 'Thailand', 'Tunisia',
                          'Turkey', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'USA', 'Uzbekistan'])
def test_resolve_country(country_name):
    cc = CountriesSyncAgent.resolve_country(country_name)
    assert cc is not None and len(cc) == 2


def test_mock_date_works():
    set_mock_date(None)
    assert mockables.today() == date.today()
    set_mock_date(date(2017, 1, 1))
    assert mockables.today() == date(2017, 1, 1)
    set_mock_date(None)
    assert mockables.today() == date.today()
