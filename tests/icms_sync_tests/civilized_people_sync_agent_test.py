import logging

from scripts.prescript_setup import db
import pytest

from scripts.sync import ForwardSyncManager, PeopleSyncAgent
from tests.testutils import recreate_common_db, recreate_people_db, assert_attr_val, set_mock_date
from tests.testutils import OldIcmsMockPersonFactory, MockUserFactory

from datetime import date
from icms_orm.cmspeople import Person as P
from icms_orm.common import Person as NP, PersonStatus as NPS


class TestData(object):

    _data = [
        {1: {P.isAuthorSuspended: False, P.isAuthorBlock: False}, 2: {P.isAuthorSuspended: False, P.isAuthorBlock: False}, 3: {P.isAuthorSuspended: True, P.isAuthorBlock: False}},
        {1: {P.isAuthorSuspended: True, P.isAuthorBlock: False}, 2: {P.isAuthorSuspended: True, P.isAuthorBlock: False}, 3: {P.isAuthorSuspended: True, P.isAuthorBlock: True}},
        {1: {P.isAuthorSuspended: True, P.isAuthorBlock: False}, 2: {P.isAuthorSuspended: False, P.isAuthorBlock: False}, 3: {P.isAuthorSuspended: True, P.isAuthorBlock: False}},
        {1: {P.isAuthorSuspended: False, P.isAuthorBlock: True}, 2: {P.isAuthorSuspended: True, P.isAuthorBlock: False}, 3: {P.isAuthorSuspended: True, P.isAuthorBlock: True}},
    ]

    @staticmethod
    def get_initial_ia_dict(cms_id):
        _dict = {P.cmsId: cms_id}
        _dict.update(TestData._data[0].get(cms_id))
        return _dict

    @staticmethod
    def cms_ids():
        return range(1, 4)

    @staticmethod
    def day_cms_ids_cartesian():
        product = []
        for day_no in range(0, len(TestData.dates())):
            for cms_id in TestData.cms_ids():
                product.append((day_no, cms_id))
        return product

    @staticmethod
    def dates():
        return [date(2017, 1, 1), date(2017, 2, 1), date(2017, 12, 1), date(2018, 1, 1)]

    @staticmethod
    def get_attr(date_or_idx, cms_id, attr):
        _idx = 0
        if isinstance(date_or_idx, int):
            _idx = date_or_idx
        else:
            _dates = TestData.dates()
            while _dates[_idx] > date_or_idx:
                _idx += 1
            while TestData._data[_idx].get(cms_id, {}).get(attr, None) is None:
                _idx -= 1
        return TestData._data[_idx].get(cms_id).get(attr)

    @staticmethod
    def get_suspended(date, cms_id):
        return TestData.get_attr(date, cms_id, P.isAuthorSuspended)

    @staticmethod
    def get_blocked(date, cms_id):
        return TestData.get_attr(date, cms_id, P.isAuthorBlock)


@pytest.fixture(scope='class')
def day(request):
    counts = (hasattr(day, 'counts') and getattr(day, 'counts') or {})
    counts_key = '{0}, {1}'.format(request.param, request.cls.__name__)
    counts[counts_key] = counts.get(counts_key, 0) + 1
    setattr(day, 'counts', counts)
    if counts[counts_key] <= 1:
        logging.info('Running the DAY FIXTURE for day %d! / stats: %s' % (request.param, counts))
        _recurring_fixture_body(request.param)
    else:
        logging.info('Skipping the DAY FIXTURE for day %d! / stats: %s' % (request.param, counts))
    return request.param


def _recurring_fixture_body(day_no):
    set_mock_date(TestData.dates()[day_no])

    if day_no == 0:
        recreate_common_db()
        recreate_people_db()
        OldIcmsMockPersonFactory.reset_overrides()

        for _cms_id in TestData.cms_ids():
            _p = OldIcmsMockPersonFactory.create_instance(instance_overrides=TestData.get_initial_ia_dict(_cms_id))
            db.session.add(_p)
            db.session.add(MockUserFactory.create_instance(person=_p))
    else:
        ppl = {_p.get(P.cmsId): _p for _p in db.session.query(P).all()}
        for _cms_id, _p in ppl.items():
            _p.set(P.isAuthorBlock, TestData.get_blocked(day_no, _cms_id))
            _p.set(P.isAuthorSuspended, TestData.get_suspended(day_no, _cms_id))
            db.session.add(_p)
    db.session.commit()
    ForwardSyncManager.launch_sync(agent_classes_override=[PeopleSyncAgent])


class TestPeopleSyncAgent(object):
    @pytest.mark.parametrize('day, cms_id', TestData.day_cms_ids_cartesian(), indirect=['day'])
    def test_block_and_suspended_flags(self, cms_id, day):
        assert_attr_val(NPS, {NPS.cms_id: cms_id, NPS.end_date: None},  {NPS.author_block: TestData.get_blocked(day, cms_id)})
        assert_attr_val(NPS, {NPS.cms_id: cms_id, NPS.end_date: None},  {NPS.epr_suspension: TestData.get_suspended(day, cms_id)})