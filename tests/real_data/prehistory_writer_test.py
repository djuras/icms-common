from icmsutils import prehistory as ph
import pytest
from icms_orm.cmspeople.people import Person, MoData, MemberActivity, PeopleFlagsAssociation as PFlagRel
from scripts.prescript_setup import db
from datetime import date


def test_commitless_changes():

    flags_to_grant = {'MISC_authorno', 'PUBC_AN'}
    flags_to_revoke = {'ICMS_absupport', 'ICMS_cmslist', 'ICMS_moa'}

    p = db.session.query(Person).filter(Person.cmsId == 9981).one()


    p.history.history = ''
    p.isAuthorSuspended = True
    p.isAuthor = False
    p.instCode = 'CERN'

    # let's make sure we're on the right page at the beginning of this exercise
    db.session.add(p)
    for flag_id in flags_to_grant:
        db.session.query(PFlagRel).filter(PFlagRel.cmsId == p.cmsId).filter(PFlagRel.flagId == flag_id).delete()
    for flag in flags_to_revoke:
        new_flag_rel = PFlagRel()
        (new_flag_rel.flagId, new_flag_rel.cmsId) = (flag, p.cmsId)
        db.session.add(new_flag_rel)
    db.session.add(p)
    db.session.commit()

    # should be setup by now

    p = db.session.query(Person).filter(Person.cmsId == 9981).one()
    flag_codes = {f.id for f in p.flags}
    for f in flags_to_grant:
        assert f not in flag_codes
    for f in flags_to_revoke:
        assert f in flag_codes

    actor = db.session.query(Person).filter(Person.cmsId == 912).one()

    writer = ph.PersonAndPrehistoryWriter(db_session=db.session, actor_person=actor, subject_person=p)

    writer.set_new_value(Person.isAuthor, True)
    writer.set_new_value(Person.isAuthorSuspended, False)
    writer.set_new_value(Person.instCode, 'DESY')

    for f in flags_to_grant:
        writer.grant_flag(f)
    for f in flags_to_revoke:
        writer.revoke_flag(f)

    for f in flags_to_grant:
        assert f in writer.flags_to_grant
    for f in flags_to_revoke:
        assert f in writer.flags_to_revoke

    writer.apply_changes(do_commit=True)

    p = db.session.query(Person).filter(Person.cmsId == 9981).one()
    flag_codes = {f.id for f in p.flags}

    assert p.isAuthor == True
    assert p.isAuthorSuspended == False
    assert getattr(p, Person.dateAuthorUnsuspension.key) == date.today()

    for flag in flags_to_grant:
        assert ('iCMSfor912_%s-added%s' % (date.today().strftime('%d/%m/%Y'), flag)) in p.history.history
        assert flag in flag_codes
    for flag in flags_to_revoke:
        assert ('iCMSfor912_%s-removed%s' % (date.today().strftime('%d/%m/%Y'), flag)) in p.history.history
        assert flag not in flag_codes

    # keeps writing empty instead of false.
    # assert ('iCMSfor912_%s-Author:False' % (date.today().strftime('%d/%m/%Y'))) in p.history.history
    assert ('iCMSfor912_%s-AuthorSuspend:True' % (date.today().strftime('%d/%m/%Y'))) in p.history.history
    assert ('iCMSfor912_%s-InstCode:CERN' % (date.today().strftime('%d/%m/%Y'))) in p.history.history

    e = None
    try:
        writer.apply_changes(False)
    except Exception as exc:
        e = exc
    assert e is not None
    # assert len(p.flags) == initial_flags_number

def test_committing_changes():
    pass


def test_MO_changes():
    actor = db.session.query(Person).filter(Person.cmsId == 3020).one()
    p, mo = db.session.query(Person, MoData).join(MoData, Person.cmsId == MoData.cmsId).filter(Person.cmsId == 9981).one()
    mo.mo2018 = 'NO'
    db.session.add(mo)
    db.session.commit()

    assert (mo.mo2018 == 'NO')
    writer = ph.PersonAndPrehistoryWriter(db_session=db.session, actor_person=actor, subject_person=p)

    writer.set_new_value(MoData.mo2018, 'YES')
    writer.apply_changes(do_commit=True)

    p, mo = db.session.query(Person, MoData).join(MoData, Person.cmsId == MoData.cmsId).filter(
        Person.cmsId == 9981).one()

    assert (mo.mo2018 == 'YES')
    assert ('iCMSfor3020_%s-MO2018:NO' % (date.today().strftime('%d/%m/%Y')) in p.history.history)
