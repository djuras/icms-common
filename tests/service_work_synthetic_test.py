#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icmsutils.icmstest import mock
from scripts.prescript_setup import db, config
import pytest
from .testutils import recreate_epr_db, close_enough, make_mock_time_line_user as tlu
from icms_orm.epr import EprInstitute as Inst, EprUser as User, TimeLineUser as TLU, Category
from collections import OrderedDict
from icmsutils.businesslogic import servicework as swk
from datetime import datetime as dt
import itertools

UFA, RIO = (1, 2)
DD, MM, JD, JS, PS, JK = range(1, 7)


class ServiceWorkSyntheticTestData(object):

    _mock_objects = []

    def __init__(self):

        def usr_data(name, cmsid, inst, is_au=True, sus=False, stat='CMS', capa='Physicist'):
            return (name.split(' ')[1].lower(), ', '.join(reversed(name.split(' '))), 1111 + cmsid, cmsid, inst, is_au, stat, sus,
                    capa, 2015, inst)

        self._scenario_data = OrderedDict(
            [(Category, (('Physicist', True), ('Doctoral-Student', True), ('Non-Doctoral Student', False))),
             (Inst, ((UFA, 'UFA', 'Russia', 'UFA', 'Yes'), (RIO, 'RIO', 'Brazil', 'RIO', 'Yes'))),
             (User, (
                 usr_data('Donald Duck', DD, UFA),
                 usr_data('Mickey Mouse', MM, UFA),
                 usr_data('Joe Doe', JD, RIO),
                 usr_data('John Smith', JS, RIO),
                 usr_data('Pedro Santos', PS, RIO),
                 usr_data('Jan Kowalski', JK, UFA))),
             (TLU, (
                 tlu(year=2015, ic='UFA', d_ap=2.56, is_au=False, cms_id=DD, yf=2.56 / 6),
                 tlu(year=2016, ic='UFA', d_ap=3.44, is_au=True, cms_id=DD, d_au=3),
                 # let's have an applicant changing from RIO to UFA in 2016 (let's have dues that add up to a weird number!)
                 tlu(year=2015, ic='RIO', d_ap=1.5, is_au=False, cms_id=MM, yf=15 / 60.0),
                 tlu(year=2016, ic='RIO', d_ap=2.11, is_au=False, cms_id=MM, yf=2.11 / (6.0 - 1.5)),
                 tlu(year=2016, ic='UFA', d_ap=1.13, is_au=False, cms_id=MM, yf=1 - (2.11 / (6.0 - 1.5))),
                 # let's have someone in RIO who changes from STU to PHY in 2016 and becomes an author in 2017
                 tlu(year=2016, ic='RIO', d_ap=0.0, is_au=False, cms_id=JD, cat=3),
                 tlu(year=2016, ic='RIO', d_ap=3.2, is_au=False, cms_id=JD, cat=1),
                 tlu(year=2017, ic='RIO', d_ap=0.8, is_au=False, cms_id=JD, cat=1),
                 tlu(year=2017, ic='RIO', d_ap=2.0, is_au=True, cms_id=JD, cat=1, d_au=2.143, yf=(2.0 / 2.8))
             ))])
        self._app_due_data = {2015: {DD: 2.56, MM: 1.5, JD: 0},
                      2016: {DD: 3.44, MM: 2.11 + 1.13, JD: 3.2},
                      2017: {DD: 0, MM: 0, JD: 2.8}}

        self._first_due_year = {DD: 2015, MM: 2015, JD: 2016}

        self._totals = {
            2015: {'UFA': 2.56, 'RIO': 1.5},
            2016: {'UFA': 7.57, 'RIO': 5.33},
            2017: {'UFA': 0.0, 'RIO': 4.943}
        }

    def setup(self):
        for klass, data in self._scenario_data.items():
            # if klass.__init__.__code__.co_argcount > 1:
            if isinstance(data[0], tuple):
                for idx, x in enumerate([klass(*data) for data in data], start=1):
                    x.id = idx
                    db.session.add(x)
            elif isinstance(data[0], dict):
                for x in [klass.from_ia_dict(data) for data in data]:
                    db.session.add(x)
            db.session.commit()
        db.session.commit()

    @property
    def totals(self):
        return self._totals

    @property
    def total_applicant_dues_before_certain_year(self):
        # tuples of (cms_id, year, due)
        result = []
        for _cms_id in [JD, DD, MM]:
            for _year in range(2015, 2018):
                result.append(
                    (_cms_id, _year, sum(self._app_due_data.get(__year).get(_cms_id) for __year in range(2015, _year))))
        return result

    @property
    def first_due_years(self):
        return [
        (_cms_id, min([_year for _year in self._app_due_data if self._app_due_data.get(_year, {}).get(_cms_id, 0) > 0])) for
        _cms_id in [JD, DD, MM]]


    @property
    def total_app_dues(self):
        return [(_cms_id, sum([self._app_due_data.get(_year, {}).get(_cms_id, 0) for _year in range(2015, 2018)])) for
                      _cms_id in [JD, DD, MM]]


test_data = ServiceWorkSyntheticTestData()


@pytest.fixture(scope='module')
def one_time_setup():
    recreate_epr_db()
    test_data.setup()


@pytest.mark.parametrize('year, inst, due', [(year, inst, test_data.totals[year][inst]) for (year, inst) in itertools.product([2015, 2016, 2017], ['UFA', 'RIO'])])
def test_inst_dues(one_time_setup, year, inst, due):
    computed_due = swk.get_institute_due(inst_code=inst, year=year, db_session=db.session)
    assert close_enough(computed_due, due, 0.01, 0.01), \
        '%s in %d should have a total EPR due of %.2f but computed %.2f' % \
        (inst, year, test_data.totals[year][inst], computed_due)


@pytest.mark.parametrize('cms_id, year, prior_due', test_data.total_applicant_dues_before_certain_year)
def test_get_total_due_prior_to_certain_year(one_time_setup, cms_id, year, prior_due):
    stats = swk.AppDueStats(db_session=db.session, current_year=year)
    assert prior_due == stats.get_total_before_this_year(cms_id)


@pytest.mark.parametrize('cms_id, first_due_year', test_data.first_due_years)
def test_get_first_application_due_year(one_time_setup, cms_id, first_due_year):
    stats = swk.AppDueStats(db_session=db.session)
    assert first_due_year == stats.get_first_due_year(cms_id)


@pytest.mark.parametrize('cms_id, total_due', test_data.total_app_dues)
def test_get_total_applicant_due(one_time_setup, cms_id, total_due):
    stats = swk.AppDueStats(db_session=db.session)
    assert total_due == stats.get_total(cms_id)

